<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
        <title>Lista pokojów</title>
        <script src="<c:url value="/resources/sockjs-0.3.4.min.js" />"></script>
        <script src="<c:url value="/resources/stomp.js" />"></script>
        <script src="<c:url value="/resources/jquery-1.11.2.min.js" />"></script>
        <script type="text/javascript">
                $(function () {
                        var appUrl = "<c:url value="/" />";
                        var socket = new SockJS('/list');
                        var stompClient = Stomp.over(socket);

                        stompClient.connect({}, function (frame) {
                                console.log(frame);

                                stompClient.subscribe('<c:url value="/user/${username}/response/list/users" />', function (result) {
                                        showUsers(JSON.parse(result.body));
                                });

                                stompClient.subscribe('<c:url value="/user/${username}/response/list/rooms" />', function (result) {
                                        showRooms(JSON.parse(result.body));
                                });
                        });

                        function showRooms(result) {
                                console.log(result);

                                $('ul#rooms').empty();
                                var toAdd;
                                $(result).each(function (index, value) {
                                        toAdd = '<li id="' + value.id + '">';

                                        toAdd += 'Temat: ' + "<a href='" + appUrl + "list/join/" + value.id + "'>" + value.subject + "</a>" + '<br />';
                                        toAdd += 'Opis: ' + value.description + '<br />';
                                        toAdd += 'Użytkownicy: ';

                                        $(value.activeUsers).each(function(ind, val) {
                                                toAdd += val + ', ';
                                        });

                                        toAdd += '</li>';

                                        $('ul#rooms').append(toAdd);
                                });
                        }

                        function showUsers(result) {
                                $('div#users').empty();
                                $('div#users').append('Użytkownicy przeglądający liste pokojów: ');
                                $(result).each(function (index, value) {
                                        $('div#users').append(value + ', ');
                                });
                        }

                        $("input[name='filter']").on("keyup", function() {
                                var inputValue = $(this).val();

                                if(inputValue.length > 0)
                                        stompClient.send("/app/list/filter", {}, $(this).val());
                                else
                                        stompClient.send("/app/list/filter/no", {});
                        });
                });


        </script>
</head>
        <body>
                <a href="<c:url value="/logout_perform" />">Wyloguj</a><br/>
                <div>
                        Wyszukaj: <input type="text" name="filter" />
                </div>

                <div id='users'>Trwa wczytywanie...</div>
                <ul id="rooms">Trwa wczytywanie...</ul>
        </body>
</html>

