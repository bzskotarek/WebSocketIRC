<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
        <title>Pokój ${idRoom}</title>
        <script src="<c:url value="/resources/sockjs-0.3.4.min.js" />"></script>
        <script src="<c:url value="/resources/stomp.js" />"></script>
        <script src="<c:url value="/resources/jquery-1.11.2.min.js" />"></script>
        <script type="text/javascript">
                $(function () {
                        var idRoom = "${idRoom}";
                        var socket = new SockJS('/roomWS');
                        var stompClient = Stomp.over(socket);

                        $("button#send").click(function() {
                                stompClient.send("/app/room/${idRoom}/message", {}, $("input#sendText").val());
                                $("input#sendText").val("");
                        });

                        stompClient.connect({}, function (frame) {
                                console.log(frame);

                                stompClient.send("/app/room/${idRoom}");

                                stompClient.subscribe('<c:url value="/user/${username}/response/room/${idRoom}/message" />', function (result) {
                                        showMessages(JSON.parse(result.body));
                                });

                                stompClient.subscribe('<c:url value="/user/${username}/response/room/${idRoom}/users" />', function (result) {
                                        showUsers(JSON.parse(result.body));
                                });
                        });

                        function showMessages(result) {
                                if(!$.isArray(result)) {
                                        $("div#chat").append(generateChatMessage(result.type, result.message));
                                        $('div#chat').scrollTop($('div#chat')[0].scrollHeight);
                                } else {
                                        $(result).each(function(index, value) {
                                                $("div#chat").append(generateChatMessage(value.type, value.message));
                                                $('div#chat').scrollTop($('div#chat')[0].scrollHeight);
                                        })
                                }
                        }

                        function showUsers(result) {
                                $('span#users').empty();
                                $(result).each(function (index, value) {
                                        $('span#users').append(value + ', ');
                                });
                        }

                        function generateChatMessage(type, messageEntity) {
                                return messageEntity + "<br />";
                        }
                });


        </script>
</head>
        <body>
                <a href="<c:url value="/list" />">< Wróć</a>
                Lista użytkowników:
                <span id="users">Trwa wczytywanie...</span>

                <div id="chat" style="height: 200px; width: 1000px; overflow-y: scroll; border: 1px solid;" > </div>
                <input id="sendText" type="text" /> <button id="send" type="button">Wyślij</button>
        </body>
</html>
