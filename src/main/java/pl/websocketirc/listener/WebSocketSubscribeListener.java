package pl.websocketirc.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import pl.websocketirc.pojo.Message;
import pl.websocketirc.websocketMessage.MessageWebsocketMessage;
import pl.websocketirc.service.MessageService;
import pl.websocketirc.service.WebsocketService;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class WebSocketSubscribeListener implements ApplicationListener<SessionSubscribeEvent> {
    @Autowired
    private WebsocketService websocketService;

    @Autowired
    private MessageService messageService;

    @Override
    public void onApplicationEvent(SessionSubscribeEvent sessionSubscribeEvent) {
        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(sessionSubscribeEvent.getMessage());
        String username = headers.getUser().getName();
        String destination = headers.getDestination();

        // tylko dla widoku listy pokojow
        if (destination.contains("/response/list/rooms")) {
            websocketService.sendRoomsListToUsersInRoom();
        } else if (destination.contains("/response/list/users")) {
            websocketService.changeUserRoom(username, -1, headers.getSessionId());
            websocketService.sendUserListToUsersOnListPage();
        }

        // tylko dla uzykownikow, ktorzy znajduja sie w pokoju
        else if (destination.contains("/response/room/")) {
            // wydobycie id pokoju
            Matcher matcher = Pattern.compile("[\\d]+").matcher(destination);
            matcher.find();
            Integer room = Integer.valueOf(matcher.group());

            if (destination.contains("/users")) {
                websocketService.changeUserRoom(username, room, headers.getSessionId());
                websocketService.sendUserListToUsersInRoomPage(room);

                websocketService.sendRoomsListToUsersInRoom();
                websocketService.sendUserListToUsersOnListPage();
            } else if (destination.contains("/message")) {
                messageService.addUserJoined(username, room);

                // wyslanie dotychczasowej historii
                List<MessageWebsocketMessage> messagesWebsocketMessages =
                        messagesToMessageWebsocketMessages(messageService.findByIdOrderByDateAscAndTimeAsc(room));

                websocketService.sendMessagesToUsersInRoom(room, messagesWebsocketMessages);
            }
        }
    }

    private List<MessageWebsocketMessage> messagesToMessageWebsocketMessages(List<Message> messages) {
        List<MessageWebsocketMessage> messageWebsocketMessages = new LinkedList<>();

        for (Message item : messages) {
            messageWebsocketMessages.add(new MessageWebsocketMessage(item));
        }

        return messageWebsocketMessages;
    }
}
