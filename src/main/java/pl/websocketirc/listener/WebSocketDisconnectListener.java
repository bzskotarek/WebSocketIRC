package pl.websocketirc.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import pl.websocketirc.pojo.Message;
import pl.websocketirc.websocketMessage.MessageWebsocketMessage;
import pl.websocketirc.service.MessageService;
import pl.websocketirc.service.WebsocketService;


public class WebSocketDisconnectListener implements ApplicationListener<SessionDisconnectEvent> {
    @Autowired
    private WebsocketService websocketService;

    @Autowired
    private MessageService messageService;

    @Override
    public void onApplicationEvent(SessionDisconnectEvent sessionDisconnectEvent) {
        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(sessionDisconnectEvent.getMessage());
        String username = headers.getUser().getName();

        Integer room = websocketService.removeUserFromRoom(username, headers.getSessionId());
        // za kazdym razem wysylamy informacje do listy pokojow, poniewaz jest na niej podglad
        // na aktywnosc uzytkownikow w kazdym pokoju
        websocketService.sendRoomsListToUsersInRoom();
        websocketService.sendUserListToUsersOnListPage();

        // logujemy obecnosc we wszystkich pokojach. "-1" odnosi sie do listy pokojów, a tam nie ma chatu
        if (room != -1) {
            Message message = messageService.addUserLeft(username, room);
            MessageWebsocketMessage messageWebsocketMessage = new MessageWebsocketMessage(message);
            websocketService.sendMessageToUsersInRoom(room, messageWebsocketMessage);
            websocketService.sendUserListToUsersInRoomPage(room);
        }
    }
}
