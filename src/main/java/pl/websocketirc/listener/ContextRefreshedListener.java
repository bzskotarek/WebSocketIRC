package pl.websocketirc.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import pl.websocketirc.pojo.User;
import pl.websocketirc.service.RoomService;
import pl.websocketirc.service.RoleService;
import pl.websocketirc.service.UserService;

@Component
public class ContextRefreshedListener implements ApplicationListener<ContextRefreshedEvent> {
    private final Logger LOGGER = LoggerFactory.getLogger(ContextRefreshedListener.class);

    private boolean configured = false;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    @Autowired
    private RoomService roomService;

    private static boolean in = false;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        // Dispatcher context bedzie odswiezany dwa razy. Bug zwiazany z wersja 4.1.6 Springa
        // rozwiazanie tymczasowe
        synchronized (this) {
            if(in)
                return;
            in = true;
        }

        if (roleService == null
                || userService == null
                || roomService == null
                || configured)
            return;

        LOGGER.info("Wstepne konfigurowanie bazy danych.");
        LOGGER.info("Dodawanie rol.");
        createRoleIfDoesntExist("ROLE_USER");
        createRoleIfDoesntExist("ROLE_ADMIN");

        LOGGER.info("Dodawanie przykladowych uzytkownikow.");
        createUserIfDoesntExist("jurek@gmail.com", "Jurek", "Mysiak", "jurek", true);
        createUserIfDoesntExist("test@test.com", "Test", "Testowniak", "test", false);
        createUserIfDoesntExist("grazyna@mail.com", "Grażyna", "Bezimienna", "grazyna", false);

        LOGGER.info("Dodawanie przykladowych pokojów.");
        createRoomIfDoesntExist(1, "Fani gier", "Pokój dla fanów gier komputerowych");
        createRoomIfDoesntExist(2, "Choroby serca", "Dla osób, które przybyły choroby serca");

        User sad = userService.findByEmail("jurek@gmail.com");

        configured = true;
    }

    private void createRoleIfDoesntExist(String role) {
        if (roleService.findRole(role) == null) {
            LOGGER.info("Nie znaleziono roli " + role + ". Dodawanie...");
            roleService.createRole(role);
        } else
            LOGGER.info("Rola " + role + " już istnieje.");
    }

    private void createUserIfDoesntExist(String email, String name, String surname, String password, boolean admin) {
        if (userService.findByEmail(email) == null) {
            LOGGER.info("Nie znaleziono użytkownika " + email + ". Dodawanie...");
            userService.add(email, name, surname, password, admin);
        } else
            LOGGER.info("Użytkonwik " + email + " już istnieje.");
    }

    private void createRoomIfDoesntExist(Integer id, String subject, String description) {
        if (roomService.findById(id) == null) {
            LOGGER.info("Nie znaleziono pokoju " + id + ". Dodawanie...");
            roomService.add(id, subject, description);
        } else
            LOGGER.info("Pokój " + id + " juz istnieje.");
    }
}
