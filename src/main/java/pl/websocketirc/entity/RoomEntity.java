package pl.websocketirc.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "rooms")
public class RoomEntity {
    @Id
    @GeneratedValue
    private Integer id;

    private Date date;

    @NotNull
    private String subject;

    @NotNull
    private String description;

    public RoomEntity() {
    }

    public RoomEntity(Integer id, String subject, String description) {
        this.id = id;
        this.subject = subject;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
