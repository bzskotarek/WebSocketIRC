package pl.websocketirc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class NavigationController {
    @RequestMapping("/")
    public String redirectToList() {
        return "redirect:/list";
    }

}
