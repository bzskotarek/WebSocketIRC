package pl.websocketirc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.websocketirc.pojo.Message;
import pl.websocketirc.websocketMessage.MessageWebsocketMessage;
import pl.websocketirc.service.MessageService;
import pl.websocketirc.service.WebsocketService;

import java.security.Principal;

@Controller
@RequestMapping("/room")
public class RoomController {
    @Autowired
    private WebsocketService websocketService;

    @Autowired
    private MessageService messageService;

    @RequestMapping("/{id}")
    public String returnView(@PathVariable String id, Model model, Principal principal) {
        model.addAttribute("username", principal.getName());
        model.addAttribute("idRoom", id);

        return "room";
    }

    @MessageMapping("/room/{id}/message")
    public void changeUserList(@DestinationVariable Integer id, String message, Principal principal) {
        Message pojoMessage = messageService.addMessage(principal.getName(), id, message);
        MessageWebsocketMessage messageWebsocketMessageJson = new MessageWebsocketMessage(pojoMessage);
        websocketService.sendMessageToUsersInRoom(id, messageWebsocketMessageJson);
    }
}
