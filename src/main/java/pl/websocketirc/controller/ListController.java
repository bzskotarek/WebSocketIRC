package pl.websocketirc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.websocketirc.service.WebsocketService;

import java.security.Principal;

@Controller
@RequestMapping("/list")
public class ListController {
    @Autowired
    private WebsocketService websocketService;

    @RequestMapping("")
    public String roomList(Model model, Principal principal) {
        model.addAttribute("username", principal.getName());

        return "list";
    }

    @RequestMapping("/join/{id}")
    public String joinRoom(@PathVariable String id) {
        return "redirect:/room/" + id;
    }

    @MessageMapping("/list/filter")
    public void filterRooms(String filter, Principal principal) {
        websocketService.changeUserFilter(principal.getName(), filter);
        websocketService.sendRoomsListToUserOnListPage(principal.getName());
    }

    @MessageMapping("/list/filter/no")
    public void filterRoomsNoFilter(Principal principal) {
        websocketService.changeUserFilter(principal.getName(), "");
        websocketService.sendRoomsListToUserOnListPage(principal.getName());
    }
}
