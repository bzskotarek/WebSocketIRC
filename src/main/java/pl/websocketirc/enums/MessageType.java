package pl.websocketirc.enums;

public enum MessageType {JOINED, MESSAGE, LEFT}