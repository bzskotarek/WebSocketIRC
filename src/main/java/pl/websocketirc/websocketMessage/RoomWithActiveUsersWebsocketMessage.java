package pl.websocketirc.websocketMessage;

import java.util.List;

public class RoomWithActiveUsersWebsocketMessage {
    private Integer id;

    private String subject;

    private String description;

    private List<String> activeUsers;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getActiveUsers() {
        return activeUsers;
    }

    public void setActiveUsers(List<String> activeUsers) {
        this.activeUsers = activeUsers;
    }

    public boolean contains(String value) {
        return subject.contains(value) || description.contains(value);
    }
}
