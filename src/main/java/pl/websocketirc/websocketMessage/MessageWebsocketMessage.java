package pl.websocketirc.websocketMessage;

import pl.websocketirc.enums.MessageType;
import pl.websocketirc.pojo.Message;

import java.text.SimpleDateFormat;

public class MessageWebsocketMessage {

    private MessageType type;

    private String message;

    public MessageWebsocketMessage(Message message) {
        this.type = message.getType();

        SimpleDateFormat simpleDateFormatDate = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat simpleDateFormatTime = new SimpleDateFormat("HH:mm:ss");
        if (type == MessageType.MESSAGE)
            this.message = new StringBuilder("[")
                    .append(simpleDateFormatDate.format(message.getDate()))
                    .append(" ")
                    .append(simpleDateFormatTime.format(message.getTime()))
                    .append("] ")
                    .append(message.getUser().getName())
                    .append(" ")
                    .append(message.getUser().getSurname())
                    .append(": ")
                    .append(message.getMessage())
                    .toString();
        else if (type == MessageType.JOINED)
            this.message = new StringBuilder("[")
                    .append(simpleDateFormatDate.format(message.getDate()))
                    .append(" ")
                    .append(simpleDateFormatTime.format(message.getTime()))
                    .append("] ")
                    .append(message.getUser().getName())
                    .append(" ")
                    .append(message.getUser().getSurname())
                    .append(" dołączył do pokoju.")
                    .toString();
        else if (type == MessageType.LEFT)
            this.message = new StringBuilder("[")
                    .append(simpleDateFormatDate.format(message.getDate()))
                    .append(" ")
                    .append(simpleDateFormatTime.format(message.getTime()))
                    .append("] ")
                    .append(message.getUser().getName())
                    .append(" ")
                    .append(message.getUser().getSurname())
                    .append(" opuścił pokój.")
                    .toString();
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
