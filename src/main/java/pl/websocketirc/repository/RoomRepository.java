package pl.websocketirc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.websocketirc.entity.RoomEntity;

public interface RoomRepository extends JpaRepository<RoomEntity, Integer> {
    RoomEntity findById(Integer id);
}
