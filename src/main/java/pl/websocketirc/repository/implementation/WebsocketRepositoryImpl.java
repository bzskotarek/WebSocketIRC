package pl.websocketirc.repository.implementation;

import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.springframework.stereotype.Repository;
import pl.websocketirc.repository.WebsocketRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class WebsocketRepositoryImpl implements WebsocketRepository {
    private Map<String, BidiMap> userAndRoom;
    private Map<String, String> userAndRoomFilter;

    public WebsocketRepositoryImpl() {
        userAndRoom = new ConcurrentHashMap<>();
        userAndRoomFilter = new ConcurrentHashMap<>();
    }

    public void changeUserRoom(String user, Integer room, String session) {
        if (userAndRoom.get(user) == null) {
            userAndRoom.put(user, new DualHashBidiMap());
            userAndRoomFilter.put(user, "");
        }

        userAndRoom.get(user).put(room, session);
    }

    public void removeUserFromRoom(String user, String session) {
        BidiMap userRooms = userAndRoom.get(user);

        userRooms.removeValue(session);

        if (userRooms.size() == 0) {
            userAndRoom.remove(user);
            userAndRoomFilter.remove(user);
        }

    }

    public Integer getRoomId(String user, String session) {
        return (Integer) userAndRoom.get(user).getKey(session);
    }

    public String getFilter(String key) {
        return userAndRoomFilter.get(key);
    }

    public void changeUserFilter(String user, String filter) {
        userAndRoomFilter.put(user, filter);
    }

    public List<String> getUsersByRoomId(Integer room) {
        List<String> users = new LinkedList<>();

        Set<Map.Entry<String, BidiMap>> set = userAndRoom.entrySet();
        for (Map.Entry<String, BidiMap> item : set) {
            if (item.getValue().containsKey(room))
                users.add(item.getKey());
        }

        return users;
    }
}
