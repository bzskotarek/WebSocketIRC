package pl.websocketirc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.websocketirc.entity.RoleEntity;

public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {
    RoleEntity findByRole(String role);
}
