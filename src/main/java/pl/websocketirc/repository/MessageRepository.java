package pl.websocketirc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.websocketirc.entity.MessageEntity;

import java.util.List;

public interface MessageRepository extends JpaRepository<MessageEntity, Integer> {
    MessageEntity findById(Integer id);

    @Query("SELECT m FROM MessageEntity m WHERE m.roomEntity.id = ?1 ORDER BY date ASC, time ASC")
    List<MessageEntity> findByRoomIdOrderByDateAscAndTimeAsc(Integer id);
}
