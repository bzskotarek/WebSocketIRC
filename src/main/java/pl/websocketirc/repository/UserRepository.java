package pl.websocketirc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.websocketirc.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {
    UserEntity findByEmail(String email);
}
