package pl.websocketirc.repository;

import java.util.List;

public interface WebsocketRepository {

    void changeUserRoom(String user, Integer room, String session);

    void removeUserFromRoom(String user, String session);

    Integer getRoomId(String user, String session);

    String getFilter(String key);

    void changeUserFilter(String user, String filter);

    List<String> getUsersByRoomId(Integer room);
}
