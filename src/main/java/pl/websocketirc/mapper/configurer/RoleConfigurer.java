package pl.websocketirc.mapper.configurer;

import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;
import pl.websocketirc.entity.RoleEntity;
import pl.websocketirc.mapper.MappingConfigurer;
import pl.websocketirc.pojo.Role;

@Component
public class RoleConfigurer implements MappingConfigurer {
    @Override
    public void configure(MapperFactory mapperFactory) {
        mapperFactory.classMap(RoleEntity.class, Role.class).byDefault().register();
    }
}
