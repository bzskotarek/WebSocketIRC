package pl.websocketirc.mapper.configurer;

import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;
import pl.websocketirc.entity.RoomEntity;
import pl.websocketirc.mapper.MappingConfigurer;
import pl.websocketirc.pojo.Room;

@Component
public class RoomConfigurer implements MappingConfigurer {
    @Override
    public void configure(MapperFactory mapperFactory) {
        mapperFactory.classMap(RoomEntity.class, Room.class).byDefault().register();
    }
}
