package pl.websocketirc.mapper.configurer;

import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;
import pl.websocketirc.entity.UserEntity;
import pl.websocketirc.mapper.MappingConfigurer;
import pl.websocketirc.pojo.User;

@Component
public class UserConfigurer implements MappingConfigurer {
    @Override
    public void configure(MapperFactory mapperFactory) {
        mapperFactory.classMap(UserEntity.class, User.class).byDefault().register();
    }
}
