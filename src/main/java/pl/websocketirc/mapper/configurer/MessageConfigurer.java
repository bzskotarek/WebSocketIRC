package pl.websocketirc.mapper.configurer;

import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;
import pl.websocketirc.entity.MessageEntity;
import pl.websocketirc.mapper.MappingConfigurer;
import pl.websocketirc.pojo.Message;

@Component
public class MessageConfigurer implements MappingConfigurer {
    @Override
    public void configure(MapperFactory mapperFactory) {
        mapperFactory.classMap(MessageEntity.class, Message.class)
                .field("userEntity", "user")
                .byDefault().register();
    }
}
