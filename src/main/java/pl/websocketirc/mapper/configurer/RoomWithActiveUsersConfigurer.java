package pl.websocketirc.mapper.configurer;

import ma.glasnost.orika.MapperFactory;
import org.springframework.stereotype.Component;
import pl.websocketirc.websocketMessage.RoomWithActiveUsersWebsocketMessage;
import pl.websocketirc.mapper.MappingConfigurer;
import pl.websocketirc.pojo.Room;

@Component
public class RoomWithActiveUsersConfigurer implements MappingConfigurer {
    @Override
    public void configure(MapperFactory mapperFactory) {
        mapperFactory.classMap(Room.class, RoomWithActiveUsersWebsocketMessage.class).byDefault().register();
    }
}
