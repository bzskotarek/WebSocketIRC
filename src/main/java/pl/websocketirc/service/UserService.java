package pl.websocketirc.service;

import pl.websocketirc.pojo.User;

public interface UserService {
    User findByEmail(String email);

    void add(String email, String name, String surname, String password, boolean admin);
}
