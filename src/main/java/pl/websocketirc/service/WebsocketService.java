package pl.websocketirc.service;

import pl.websocketirc.websocketMessage.MessageWebsocketMessage;

import java.util.List;

public interface WebsocketService {
    void changeUserRoom(String user, Integer room, String session);

    Integer removeUserFromRoom(String user, String session);

    void changeUserFilter(String user, String filter);

    void sendRoomsListToUsersInRoom();

    void sendRoomsListToUserOnListPage(String username);

    void sendUserListToUsersOnListPage();

    void sendUserListToUsersInRoomPage(Integer room);

    void sendMessageToUsersInRoom(Integer room, MessageWebsocketMessage messageWebsocketMessage);

    void sendMessagesToUsersInRoom(Integer room, List<MessageWebsocketMessage> messagesWebsocketMessages);
}
