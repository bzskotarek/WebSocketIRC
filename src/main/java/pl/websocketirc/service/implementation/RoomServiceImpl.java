package pl.websocketirc.service.implementation;

import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.websocketirc.entity.RoomEntity;
import pl.websocketirc.pojo.Room;
import pl.websocketirc.repository.RoomRepository;
import pl.websocketirc.service.RoomService;

import java.util.List;


@Service
@Transactional(readOnly = true)
public class RoomServiceImpl implements RoomService {
    private final Logger LOGGER = LoggerFactory.getLogger(RoomServiceImpl.class);

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private MapperFacade mapperFacade;

    @Override
    @Transactional(readOnly = false)
    public void add(Integer id, String subject, String description) {
        RoomEntity roomEntity = roomRepository
                .save(new RoomEntity(id, subject, description));
    }

    @Override
    public List<Room> findAll() {
        return mapperFacade.mapAsList(roomRepository.findAll(), Room.class);
    }

    @Override
    public Room findById(Integer id) {
        return mapperFacade.map(roomRepository.findById(id), Room.class);
    }
}
