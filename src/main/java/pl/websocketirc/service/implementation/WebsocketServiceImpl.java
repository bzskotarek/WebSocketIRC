package pl.websocketirc.service.implementation;

import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import pl.websocketirc.pojo.Room;
import pl.websocketirc.websocketMessage.MessageWebsocketMessage;
import pl.websocketirc.websocketMessage.RoomWithActiveUsersWebsocketMessage;
import pl.websocketirc.repository.WebsocketRepository;
import pl.websocketirc.service.RoomService;
import pl.websocketirc.service.WebsocketService;

import java.util.*;

@Service
public class WebsocketServiceImpl implements WebsocketService {

    @Autowired
    private WebsocketRepository websocketRepository;

    @Autowired
    private RoomService roomService;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private MapperFacade mapperFacade;

    @Override
    public void changeUserRoom(String user, Integer room, String session) {
        websocketRepository.changeUserRoom(user, room, session);
    }

    @Override
    public Integer removeUserFromRoom(String user, String session) {
        Integer room = websocketRepository.getRoomId(user, session);
        websocketRepository.removeUserFromRoom(user, session);
        return room;
    }

    @Override
    public void sendRoomsListToUsersInRoom() {
        // wyslanie aktualnego stanu aktywnych uzytkownikow tylko do tych na stronie listy przypadkow
        List<String> activeUserOnRoomsPage = websocketRepository.getUsersByRoomId(-1);
        List<RoomWithActiveUsersWebsocketMessage> roomsWithActiveUserWebsocketMessages = getRoomsList();
        for (String item : activeUserOnRoomsPage) {
            simpMessagingTemplate.convertAndSendToUser(item,
                    "/response/list/rooms",
                    filterRoom(roomsWithActiveUserWebsocketMessages, websocketRepository.getFilter(item)));
        }
    }

    @Override
    public void sendRoomsListToUserOnListPage(String username) {
        simpMessagingTemplate.convertAndSendToUser(username,
                "/response/list/rooms",
                filterRoom(getRoomsList(), websocketRepository.getFilter(username)));
    }

    @Override
    public void sendUserListToUsersOnListPage() {
        List<String> activeUserOnRoomsPage = websocketRepository.getUsersByRoomId(-1);
        for (String item : activeUserOnRoomsPage) {
            simpMessagingTemplate.convertAndSendToUser(item,
                    "/response/list/users",
                    activeUserOnRoomsPage);
        }
    }

    @Override
    public void sendUserListToUsersInRoomPage(Integer room) {
        List<String> activeUserOnRoomsPage = websocketRepository.getUsersByRoomId(room);
        for (String item : activeUserOnRoomsPage) {
            simpMessagingTemplate.convertAndSendToUser(item,
                    "/response/room/" + room + "/users",
                    activeUserOnRoomsPage);
        }
    }

    @Override
    public void sendMessageToUsersInRoom(Integer room, MessageWebsocketMessage messageWebsocketMessage) {
        List<String> users = websocketRepository.getUsersByRoomId(room);

        for(String username : users) {
            simpMessagingTemplate.convertAndSendToUser(username,
                    "/response/room/" + room + "/message",
                    messageWebsocketMessage);
        }
    }

    @Override
    public void sendMessagesToUsersInRoom(Integer room, List<MessageWebsocketMessage> messagesWebsocketMessages) {
        List<String> users = websocketRepository.getUsersByRoomId(room);

        for(String username : users) {
            simpMessagingTemplate.convertAndSendToUser(username,
                    "/response/room/" + room + "/message",
                    messagesWebsocketMessages);
        }
    }

    @Override
    public void changeUserFilter(String user, String filter) {
        websocketRepository.changeUserFilter(user, filter);
    }

    private List<RoomWithActiveUsersWebsocketMessage> filterRoom(List<RoomWithActiveUsersWebsocketMessage> roomsWithActiveUserWebsocketMessages, String filter) {
        List<RoomWithActiveUsersWebsocketMessage> selectedRooms = new LinkedList<>();

        for(RoomWithActiveUsersWebsocketMessage item : roomsWithActiveUserWebsocketMessages) {
            if(item.contains(filter))
                selectedRooms.add(item);
        }

        return selectedRooms;
    }

    private List<RoomWithActiveUsersWebsocketMessage> getRoomsList() {
        List<Room> rooms = roomService.findAll();
        List<RoomWithActiveUsersWebsocketMessage> roomsWithActiveUsers = mapperFacade.mapAsList(rooms, RoomWithActiveUsersWebsocketMessage.class);

        for(RoomWithActiveUsersWebsocketMessage item : roomsWithActiveUsers) {
            item.setActiveUsers(websocketRepository.getUsersByRoomId(item.getId()));
        }

        return roomsWithActiveUsers;
    }
}
