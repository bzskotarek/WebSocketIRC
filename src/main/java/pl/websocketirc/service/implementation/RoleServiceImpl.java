package pl.websocketirc.service.implementation;

import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.websocketirc.entity.RoleEntity;
import pl.websocketirc.pojo.Role;
import pl.websocketirc.repository.RoleRepository;
import pl.websocketirc.service.RoleService;


@Service
@Transactional(readOnly = true)
public class RoleServiceImpl implements RoleService {
    private final Logger LOGGER = LoggerFactory.getLogger(RoleServiceImpl.class);

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private MapperFacade mapperFacade;

    @Override
    public Role findRole(String role) {
        return mapperFacade.map(roleRepository.findByRole(role), Role.class);
    }

    @Override
    @Transactional(readOnly = false)
    public void createRole(String role) {
        roleRepository.save(new RoleEntity(role));
    }
}
