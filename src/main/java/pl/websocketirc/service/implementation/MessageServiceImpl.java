package pl.websocketirc.service.implementation;

import ma.glasnost.orika.MapperFacade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.websocketirc.entity.MessageEntity;
import pl.websocketirc.entity.UserEntity;
import pl.websocketirc.enums.MessageType;
import pl.websocketirc.entity.RoomEntity;
import pl.websocketirc.pojo.Message;
import pl.websocketirc.repository.RoomRepository;
import pl.websocketirc.repository.MessageRepository;
import pl.websocketirc.repository.UserRepository;
import pl.websocketirc.service.MessageService;

import java.util.Calendar;
import java.util.List;


@Service
@Transactional(readOnly = true)
public class MessageServiceImpl implements MessageService {
    private final Logger LOGGER = LoggerFactory.getLogger(MessageServiceImpl.class);

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private MapperFacade mapperFacade;

    @Override
    @Transactional(readOnly = false)
    public void addUserJoined(String user, Integer room) {
        UserEntity repUserEntity = userRepository.findByEmail(user);

        add("", MessageType.JOINED, user, room);
    }

    @Override
    @Transactional(readOnly = false)
    public Message addUserLeft(String user, Integer room) {
        return add("", MessageType.LEFT, user, room);
    }

    @Override
    @Transactional(readOnly = false)
    public Message addMessage(String user, Integer room, String message) {
        return add(message, MessageType.MESSAGE, user, room);
    }

    @Override
    public List<Message> findByIdOrderByDateAscAndTimeAsc(Integer id) {
        return mapperFacade.mapAsList(messageRepository.findByRoomIdOrderByDateAscAndTimeAsc(id), Message.class);
    }

    @Transactional(readOnly = false)
    private Message add(String message, MessageType type, String user, Integer room){
        UserEntity repUserEntity = userRepository.findByEmail(user);
        RoomEntity repRoomEntity = roomRepository.findOne(room);
        MessageEntity messageEntity = messageRepository.save(
                new MessageEntity(Calendar.getInstance().getTime(), message, type, repUserEntity, repRoomEntity));

        return mapperFacade.map(messageEntity, Message.class);
    }
}
