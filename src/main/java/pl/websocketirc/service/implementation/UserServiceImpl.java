package pl.websocketirc.service.implementation;

import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.websocketirc.entity.UserEntity;
import pl.websocketirc.pojo.User;
import pl.websocketirc.repository.RoleRepository;
import pl.websocketirc.repository.UserRepository;
import pl.websocketirc.service.UserService;


@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MapperFacade mapperFacade;

    @Override
    public User findByEmail(String email) {
        return mapperFacade.map(userRepository.findByEmail(email), User.class);
    }

    @Override
    @Transactional(readOnly = false)
    public void add(String email, String name, String surname, String password, boolean admin) {
        UserEntity userEntity = new UserEntity(email, name, surname, passwordEncoder.encode(password));

        userEntity.addRole(roleRepository.findByRole("ROLE_USER"));
        if (admin)
            userEntity.addRole(roleRepository.findByRole("ROLE_ADMIN"));

        userRepository.save(userEntity);
    }


}
