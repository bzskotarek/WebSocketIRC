package pl.websocketirc.service;

import pl.websocketirc.pojo.Message;

import java.util.List;

public interface MessageService {
    void addUserJoined(String user, Integer room);

    Message addUserLeft(String user, Integer room);

    Message addMessage(String user, Integer room, String message);

    List<Message> findByIdOrderByDateAscAndTimeAsc(Integer id);
}
