package pl.websocketirc.service;

import pl.websocketirc.pojo.Role;

public interface RoleService {
    Role findRole(final String role);

    void createRole(final String role);
}
