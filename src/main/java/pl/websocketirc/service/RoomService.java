package pl.websocketirc.service;

import pl.websocketirc.pojo.Room;

import java.util.List;

public interface RoomService {
        void add(Integer id, String subject, String description);

        List<Room> findAll();

        Room findById(Integer id);
}
